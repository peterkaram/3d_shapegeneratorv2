﻿using UnityEngine;
using System.Collections;


public class Controller : MonoBehaviour {

    Shape shape = new Shape();
    Cube cube = new Cube();
    Cone cone = new Cone();
    Sphere sphere = new Sphere();
    Material mat = new Material();
    ShapeClone clone = new ShapeClone();
    int select;
    PingPong2D align = new PingPong2D();
    float rSliderValue, bSliderValue, gSliderValue;
    
    void Start ()
    {
        align.SetValues(-4, 12, 4, 0.9f, -1);
        clone.LoadPingPong(align);
        shape.LoadObject(cube);
    }

    void OnGUI () {

        if (GUI.Button(new Rect(Screen.width / 2 - 200, 10, 150, 50), "Save"))
        {
            clone.LoadObject(shape.PopObject());
            clone.SetPosition();
        }
        if (GUI.Button(new Rect(Screen.width / 2 + 150, 10, 150, 50), "Shape"))
        {
            switch (select)
            {
                case 0:
                    shape.LoadObject(sphere);
                    select=1;
                    break;
                case 1:
                    shape.LoadObject(cone);
                    select=2;
                    break;
                case 2:
                    shape.LoadObject(cube);
                    select=0;
                    break;
            }              
        }
        rSliderValue = GUI.HorizontalSlider(new Rect(Screen.width / 2, 25, 100, 30), rSliderValue, 0.0F, 255.0F);
        bSliderValue = GUI.HorizontalSlider(new Rect(Screen.width / 2, 50, 100, 30), bSliderValue, 0.0F, 255.0F);
        gSliderValue = GUI.HorizontalSlider(new Rect(Screen.width / 2, 75, 100, 30), gSliderValue, 0.0F, 255.0F);
        mat.AddMaterial(shape.PopObject(), new Color32((byte)rSliderValue, (byte)bSliderValue, (byte)gSliderValue, 1));

    }
}
