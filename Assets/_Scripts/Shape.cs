﻿using UnityEngine;
using System.Collections;
    
public class Shape :MonoBehaviour  {
    
   protected GameObject activeObject;
   protected Vector3 position;
   protected Vector3 scale;



    public void LoadObject(ShapeInterface ShapeObject)
    {
        Destroy(activeObject);
        activeObject = ShapeObject.Select();
        SetPosition(position);
        SetScale(scale);
        
    }
    
 
   
    public void SetPosition(Vector3 position)
    {
        this.position = position;
            Apply();       
    }
    

    public void SetScale(Vector3 scale)
    {
        this.scale = scale;
            Apply();
    }

    protected void Apply()
    {
        if (activeObject != null)
        {
            activeObject.transform.position = position;
            activeObject.transform.localScale += scale;
        }
    }
    public GameObject PopObject()
    {
        return activeObject;
    }
}
