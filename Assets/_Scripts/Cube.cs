﻿using UnityEngine;
using System.Collections;

public class Cube : MonoBehaviour, ShapeInterface
{

    public GameObject Select()
    {
        return GameObject.CreatePrimitive(PrimitiveType.Cube);
    }
}