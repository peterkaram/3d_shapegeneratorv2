﻿using UnityEngine;
using System.Collections;

public class Material : MonoBehaviour
{

    public void AddMaterial(GameObject selectedShape, Color32 selectedColor)
    {
        UnityEngine.Material newMaterial = new UnityEngine.Material(Shader.Find("Standard"));
        newMaterial.color = selectedColor;
        selectedShape.GetComponent<Renderer>().material = newMaterial;

    }

}
