﻿using UnityEngine;
using System.Collections;

public class ShapeClone : Shape
{
    PingPong2D pose;
    Vector2 poseValues = new Vector2();

    public void LoadObject(GameObject target)
    {
        activeObject = Instantiate(target);
        SetPosition(position);
        SetScale(scale);
    }
    public void LoadPingPong(PingPong2D pose)
    {
        this.pose = pose;
    }
    public void SetPosition()
    {
        poseValues = pose.GetValues();
        position.x = poseValues.x;
        position.z = poseValues.y;
        Apply();
        pose.Move();
        
    }
}
