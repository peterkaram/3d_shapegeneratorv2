﻿using UnityEngine;
using System.Collections;

public class PingPong2D 
{
   private float xStart, yStart, xValue, yValue, xEnd, xMove, yMove;

    public void SetValues(float xStart, float yStart, float xEnd, float xMove, float yMove)
    {
        this.xValue = xStart;
        this.yValue = yStart;
        this.xStart = xStart;
        this.yStart = yStart;
        this.xEnd = xEnd;
        this.xMove = xMove;
        this.yMove = yMove;

    }
    public void Move()
    {

        xValue += xMove;
        Check();




    }

    private void Check()
    {
        if (xStart < xEnd)
        {
            if (xValue > xEnd)
            {
                ResetValues();
            }

        }
        if (xStart > xEnd)
        {
            if (xValue < xEnd)
            {
                ResetValues();
            }

        }
    }
    private void ResetValues()
    {
        xValue = xStart;
        yValue += yMove;
    }

    public Vector2 GetValues()
    {
        return new Vector2(xValue, yValue);
    }

}
