3D Shape Generator 2 Created With Unity Game Studio.

* Add Shapes, switch Colors and save instances of the shapes you like.
* Project is created with Unity 5.4.2f2, C#
* Project can be built for multiple platforms
* Date, 20/11/2016
* Version 2.0


### Updates ###

* First attempt to write SOLID code
* Color button replaced with RGB sliders

### Guidelines ###
* New shapes can be addded via inherting/extending shapeinterface
* Controller.cs : Game Loop
* Shape.cs : manages active shape 
* ShapeClone.cs : extends Shape.cs while adding the ability to clone and automatically pose objects 
* PingPong2D.cs : pingpongs a value between two values while adding new rows
* Material.cs : Creates a material with the selected color and adds the material to the selected Object


### Screenshots ###
![ShapeGeneratorV2_firstScreenShot.png](https://bitbucket.org/repo/x4rxoM/images/3347301397-ShapeGeneratorV2_firstScreenShot.png)